package stacklab.nickdnepr.com.mytmc.utils

import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.web.client.RestTemplate
import java.io.InputStream
import java.lang.reflect.Type
import java.net.HttpURLConnection
import java.net.URL
import stacklab.nickdnepr.com.mytmc.utils.Runnable.HttpMethod.GET
import stacklab.nickdnepr.com.mytmc.utils.Runnable.HttpMethod.POST
import java.io.IOException

typealias JObjectMap = HashMap<String, Any>

data class Runnable(
    val url: String,
    val restTemplate: RestTemplate
) {
    fun<T> get(type: Type?): Pair<T?, Exception?> {
        return fetch(GET,
            type = type
        )
    }

    inline fun<reified T: Any> post(entity: Any): Pair<T?, Exception?> {
        return post(
            entity,
            T::class.java
        )
    }

    fun<T> post(entity: Any, type: Type?): Pair<T?, Exception?> {
        return fetch(POST,
            entity = entity,
            type = type
        )
    }

    fun<T> fetch(method: HttpMethod, type: Type?,
                 entity: Any? = null
    ): Pair<T?, Exception?> {
        val url = URL(url)

        with(url.openConnection() as HttpURLConnection) {
            requestMethod = method.name
            setRequestProperty("Content-Type", "application/json;utf-8")

            if (method == POST && entity != null) {
                val json = if (entity is JsonConvertible)
                    entity.toJson()
                else
                    GsonBuilder().setLenient().create().toJson(entity)
                Log.d("app", json)
                outputStream.write(json.toByteArray())
            }

            try { // IO call
                if (responseCode !in 200..299) {
                    val error = errorStream
                        .tryReadText("Network error: HTTP $responseCode")
                    return null to Exception(error)
                }

                val response = inputStream.tryReadText()
                val result = GsonBuilder().setLenient().create().fromJson<T>(response, type)

                return result to null
            } catch (io: IOException) {
                return null to io
            } catch (json: JsonIOException) {
                return null to null
            }
        }
    }

    private fun InputStream.tryReadText(default: String = ""): String {
        return try {
            reader().readText()
        } catch (_: Throwable) {
            default
        }
    }

    enum class HttpMethod {
        GET, POST
    }
}