package stacklab.nickdnepr.com.mytmc.ui.tmc_list

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.beauty_item_tmc.view.*
import stacklab.nickdnepr.com.mytmc.R
import stacklab.nickdnepr.com.mytmc.models.Tmc

class TmcListAdapter(private val context: Context, var tmcList: List<Tmc>, val clickListener: (Tmc) -> Unit) :
    RecyclerView.Adapter<TmcListAdapter.TmcViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TmcViewHolder(
        LayoutInflater.from(context).inflate(
            R.layout.beauty_item_tmc, parent, false
        )
    )

    override fun getItemCount() = tmcList.size

    override fun onBindViewHolder(holder: TmcViewHolder, position: Int) {
        val model = tmcList[position]

        holder.title.text = model.items
        holder.backgroundView.setOnClickListener {
            clickListener(model)
        }

        holder.location.text = "<" + model.location_id + ">"
        holder.count.text = "0 pc."
        holder.supplier.text = if (model.supplier.isNullOrEmpty()) "none" else model.supplier
        holder.barcode.text = model.code?.textCode

        if (!model.description.isNullOrEmpty()) {
            holder.description.text = model.description
            holder.descriptionImageView.visibility = View.VISIBLE
        } else {
            holder.descriptionImageView.visibility = View.INVISIBLE
        }

        if (model.code.id == 0L) {
            holder.barcodeImageView.setImageResource(R.drawable.ic_barcode_err)
        } else {
            holder.barcodeImageView.setImageResource(R.drawable.ic_barcode)
        }

        holder.barcode.viewTreeObserver.addOnGlobalLayoutListener {
            onViewAttachedToWindow(holder)
        }
        holder.barcodeImageView.viewTreeObserver.addOnGlobalLayoutListener {
            onViewAttachedToWindow(holder)
        }
    }

    override fun onViewAttachedToWindow(holder: TmcViewHolder) {
        super.onViewAttachedToWindow(holder)
        val w1 = holder.barcodeImageView.width * 0.85f
        val w2 = holder.barcode.width.toFloat()

        if (holder.barcode.textScaleX == Float.NaN
            || holder.barcode.textScaleX <= 1f && w1 > 0 && w2 > 0)
            holder.barcode.textScaleX = w1 / w2
    }

    class TmcViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.title!!
        val location: TextView = itemView.location!!
        val supplier: TextView = itemView.supplier!!
        val count: TextView = itemView.count!!

        val backgroundView: RelativeLayout = itemView.findViewById(R.id.background)

        val description: TextView = itemView.description!!
        val descriptionImageView: AppCompatImageView = itemView.descriptionImageView!!

        val barcode: TextView = itemView.barcode!!
        val barcodeImageView: AppCompatImageView = itemView.barcodeImageView!!
    }
}