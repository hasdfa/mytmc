package stacklab.nickdnepr.com.mytmc.utils

interface JsonConvertible {
    fun toJson(): String
    fun fromJson(json: String) { /* empty */ }
}