package stacklab.nickdnepr.com.mytmc

import android.app.Application
import org.koin.android.ext.android.startKoin
import org.koin.standalone.KoinComponent
import stacklab.nickdnepr.com.mytmc.di.repositoryModule
import stacklab.nickdnepr.com.mytmc.di.viewModelModule
import com.bumptech.glide.*


class MyTmcApp : Application(), KoinComponent {

    private val modules = listOf(viewModelModule, repositoryModule)

    override fun onCreate() {
        super.onCreate()
        Glide.get(applicationContext)
            .setMemoryCategory(MemoryCategory.HIGH)
        startKoin(this, modules)
    }
}