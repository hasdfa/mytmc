package stacklab.nickdnepr.com.mytmc.ui.scanner

import android.util.Log
import androidx.lifecycle.ViewModel
import stacklab.nickdnepr.com.mytmc.repository.BarcodeRepository

class ScannerFragmentViewModel(private val barcodeRepository: BarcodeRepository) :ViewModel(){

    fun processBarcode(barcode: String) {
        Log.i("Barcode", "ViewModel processed $barcode")
        barcodeRepository.handleBarcode(barcode)
    }
}