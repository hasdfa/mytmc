package stacklab.nickdnepr.com.mytmc.ui.photo_pager

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_photo_pager.*
import stacklab.nickdnepr.com.mytmc.R
import stacklab.nickdnepr.com.mytmc.architecture.BaseMVVMFragment

class PhotoPagerFragment :BaseMVVMFragment(R.layout.fragment_photo_pager){

    private lateinit var adapter: PagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val links = arguments?.getStringArrayList("links")
        adapter = PagerAdapter(links!!, childFragmentManager)
//        showToast("${pager.adapter?.count}")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pager.adapter=adapter
    }
}