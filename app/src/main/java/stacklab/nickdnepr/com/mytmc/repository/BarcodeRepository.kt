package stacklab.nickdnepr.com.mytmc.repository

import android.util.Log

class BarcodeRepository {

    private var subscriptions: MutableMap<String, (String) -> Unit> = mutableMapOf()

    fun subscribe(tag: String, subscription: (String) -> Unit) {
        subscriptions[tag] = subscription
    }

    fun unsubscribe(tag: String){
        subscriptions.remove(tag)
    }

    fun handleBarcode(barcode: String){
        Log.i("Barcode", "repository processed $barcode  size is ${subscriptions.size}")
//        Log.i("Barcode", "")

        subscriptions.values.forEach {
            it(barcode)
        }
    }


}