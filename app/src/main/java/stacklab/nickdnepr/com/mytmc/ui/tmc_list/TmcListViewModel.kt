package stacklab.nickdnepr.com.mytmc.ui.tmc_list

import android.util.Log
import androidx.lifecycle.MutableLiveData
import stacklab.nickdnepr.com.mytmc.architecture.BaseViewModel
import stacklab.nickdnepr.com.mytmc.models.Tmc
import stacklab.nickdnepr.com.mytmc.repository.BarcodeRepository
import stacklab.nickdnepr.com.mytmc.repository.TmcRepository
import java.lang.Exception

class TmcListViewModel(private val tmcRepository: TmcRepository, private val barcodeRepository: BarcodeRepository) :
    BaseViewModel() {

    val processedCode = MutableLiveData<String>()
    val receivedTmc = MutableLiveData<Tmc>()

    init {
        Log.i("Barcode", "init viewModel")
        subscribe()
    }

    fun setErrorHandler(handler: (Exception) -> Unit) {
        tmcRepository.handleError = handler
    }

    val tmcList = MutableLiveData<List<Tmc>>()

    fun getList() {
        showLoading()
        tmcRepository.getTmcList {
            tmcList.postValue(it)
            hideLoading()
        }
    }

    fun subscribe() {
        barcodeRepository.subscribe("TmcList") {
            Log.i("Barcode", "posting new value")
            processedCode.postValue(it)
        }
    }

    fun unsubscribe() {
        barcodeRepository.unsubscribe("TmcList")
    }

    fun getTmcByCode(code: String) {
        tmcRepository.getTmcByCode(code) {
            receivedTmc.postValue(it)
        }
    }
}