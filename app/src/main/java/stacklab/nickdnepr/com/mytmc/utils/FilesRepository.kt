package stacklab.nickdnepr.com.mytmc.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import java.io.*
import java.nio.file.Files
import java.nio.file.attribute.FileAttribute

object FilesRepository {

    private val cacheDirPath by lazy {
        System.getProperty("java.io.tmpdir")
    }

    private val cacheDir by lazy {
        File(cacheDirPath)
    }

    public fun push(name: String, bytes: ByteArray, overwrite: Boolean = true) {
        val file = File(cacheDir, name)
        if (overwrite && file.exists())
            file.delete()

        if (!file.exists())
            file.createNewFile()

        file.writeBytes(bytes)
    }

    public fun getBytes(name: String): ByteArray? {
        val file = File(cacheDir, name)
        if (file.exists())
            return file.readBytes()

        return null
    }

    public fun getStream(name: String): FileInputStream? {
        val file = File(cacheDir, name)
        if (file.exists())
            return file.inputStream()

        return null
    }

    public fun getImage(name: String): Bitmap? {
        val bytes = getBytes(name)
        if (bytes != null)
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)

        return null
    }

}