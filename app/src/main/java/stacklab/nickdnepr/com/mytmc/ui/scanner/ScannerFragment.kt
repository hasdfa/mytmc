package stacklab.nickdnepr.com.mytmc.ui.scanner

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.dm7.barcodescanner.zbar.ZBarScannerView
import org.koin.androidx.viewmodel.ext.android.viewModel
import stacklab.nickdnepr.com.mytmc.architecture.BaseMVVMFragment

class ScannerFragment : BaseMVVMFragment(0) {


    private lateinit var mScannerView: ZBarScannerView
    private val viewModel:ScannerFragmentViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mScannerView = ZBarScannerView(activity?.applicationContext)
        return mScannerView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScanner()
    }

    private fun setupScanner() {
        mScannerView.flash = false
        mScannerView.setAutoFocus(true)
    }

    override fun onResume() {
        Log.i("Barcode", "Scanner onResume")
        super.onResume()
        mScannerView.setResultHandler {
            Log.i("Barcode", "scanner handled")
//            showToast(it?.contents ?: "NULL")
            viewModel.processBarcode(it!!.contents)
//            (activity as MainActivity).changeFragment(TmcListFragment(), false)
            activity?.onBackPressed()
        }
        mScannerView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()
    }

}