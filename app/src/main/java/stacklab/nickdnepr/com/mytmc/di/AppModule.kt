package stacklab.nickdnepr.com.mytmc.di

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import stacklab.nickdnepr.com.mytmc.repository.*
import stacklab.nickdnepr.com.mytmc.ui.details.TmcDetailsViewModel
import stacklab.nickdnepr.com.mytmc.ui.login.LoginViewModel
import stacklab.nickdnepr.com.mytmc.ui.main.MainViewModel
import stacklab.nickdnepr.com.mytmc.ui.scanner.ScannerFragmentViewModel
import stacklab.nickdnepr.com.mytmc.ui.tmc_list.TmcListViewModel

val viewModelModule = module {
    viewModel { LoginViewModel(get()) }
    viewModel { MainViewModel() }
    viewModel { TmcListViewModel(get(), get()) }
    viewModel { ScannerFragmentViewModel(get()) }
    viewModel { TmcDetailsViewModel(get(), get()) }
}

val repositoryModule = module {
    single { HelloRepository() }
    single { LoginRepository() }
    single { TmcRepository() }
    single { BarcodeRepository() }
}