package stacklab.nickdnepr.com.mytmc.ui.main

import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import stacklab.nickdnepr.com.mytmc.architecture.BaseFragment
import stacklab.nickdnepr.com.mytmc.architecture.BaseMVVMActivity
import stacklab.nickdnepr.com.mytmc.ui.tmc_list.TmcListFragment
import stacklab.nickdnepr.com.mytmc.R
import kotlin.math.roundToInt

class MainActivity : BaseMVVMActivity(R.layout.activity_main) {

    private val viewModel: MainViewModel by viewModel()

    override fun onBindLiveData() {
        super.onBindLiveData()
        changeFragment(TmcListFragment(), false)
    }

    private var isPanelOpened = false

    private var animation: TranslateAnimation? = null
    private var bottom = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        menuItem.setOnClickListener {
            if (isPanelOpened) close()
            else open()
        }
        draggablePanel.viewTreeObserver.addOnGlobalLayoutListener {
            if (bottom < 0 && draggablePanel.height > 0) {
                bottom = (draggablePanel.height * 0.9).roundToInt()
                Log.d("app", "Set bottom: $bottom")
            }
        }
    }

    private fun open() {
        if (isPanelOpened)
            return
        drag(0, bottom)
    }

    private fun close() {
        if (isPanelOpened.not())
            return
        drag(bottom, 0)
    }

    private fun drag(from: Int, to: Int, after: () -> Unit = {}) {
        if (animation?.hasEnded() == false)
            animation?.reset()

        animation = TranslateAnimation(
            0f, 0f,
            from.toFloat(), to.toFloat()
        )
        animation?.duration = 500
        animation?.fillAfter = true
        animation?.fillBefore = true

        animation?.setAnimationListener(object: Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {}
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {
                isPanelOpened = isPanelOpened.not()
                after()
                //draggablePanel.animate()
                //    .translationY(delta.toFloat())
            }
        })
        draggablePanel.startAnimation(animation)
    }

    fun changeFragment(fragment: BaseFragment, addToBackStack:Boolean) {
        val fTrans = supportFragmentManager.beginTransaction()
        fTrans.replace(R.id.container, fragment)
        if (addToBackStack){
            fTrans.addToBackStack("${fragment.javaClass}")
        }
        fTrans.commit()
    }
}
