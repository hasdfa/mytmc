package stacklab.nickdnepr.com.mytmc.architecture

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

abstract class BaseMVVMActivity(@LayoutRes layoutResId: Int) : BaseActivity(layoutResId) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onBindLiveData()
    }

    protected open fun onBindLiveData(){

    }

    fun<T, LD: LiveData<T>> observe(liveData:LD, onChanged:(T)->Unit){
        liveData.observe(this, Observer { value->
            value?.let(onChanged)
        })
    }
}