package stacklab.nickdnepr.com.mytmc.ui.details

import android.util.Log
import androidx.lifecycle.MutableLiveData
import stacklab.nickdnepr.com.mytmc.architecture.BaseViewModel
import stacklab.nickdnepr.com.mytmc.models.AbstractCode
import stacklab.nickdnepr.com.mytmc.models.Tmc
import stacklab.nickdnepr.com.mytmc.repository.BarcodeRepository
import stacklab.nickdnepr.com.mytmc.repository.TmcRepository
import java.io.File
import java.lang.Exception
import kotlin.reflect.KMutableProperty

class TmcDetailsViewModel(private val barcodeRepository: BarcodeRepository, private val tmcRepository: TmcRepository) : BaseViewModel() {

    val codeProcessed = MutableLiveData<String>()
    val codeReceived = MutableLiveData<AbstractCode>()
    val tmcUpdated = MutableLiveData<Tmc>()
    val photoAdded = MutableLiveData<String>()
    val imagesLoaded = MutableLiveData<List<String>>()
    val previewLoaded = MutableLiveData<String>()
    val refreshedTmc = MutableLiveData<Tmc>()

    init {
        subscribe()
    }

    var errorHandler: (Exception) -> Unit
        get() {
            return tmcRepository.handleError
        }
        set(value) {
            tmcRepository.handleError = value
        }

    fun subscribe(){
        barcodeRepository.subscribe("TmcDetails"){
            Log.i("Barcode", "posting new value")
            codeProcessed.postValue(it)
        }
    }

    fun unsubscribe(){
        barcodeRepository.unsubscribe("TmcDetails")
    }

    fun attachCode(tmc: Tmc){
        tmcRepository.attachCode(tmc) {
            tmcUpdated.postValue(it)
        }
    }

    fun getCode(textCode:String){
        tmcRepository.getCode(textCode) {
            codeReceived.postValue(it)
        }
    }

    fun addPhoto(tmcId:Long, photo:File){
        showLoading()
        tmcRepository.addPhoto(tmcId, photo) {
            photoAdded.postValue("${photo.name} uploaded")
            hideLoading()
        }
    }

    fun loadImages(tmcId: Long){
        showLoading()
        tmcRepository.loadImages(tmcId) {
            imagesLoaded.postValue(it)
            hideLoading()
        }
    }

    fun loadPreview(tmcId: Long){
        showLoading()
        tmcRepository.loadImages(tmcId) {
            if (!it.isEmpty()){
                previewLoaded.postValue(it.last())
            }
            hideLoading()
        }
    }

    fun refreshTmc(tmc:Tmc){
        showLoading()
        try {
            tmcRepository.getTmcByCode(tmc.code.textCode!!){
                refreshedTmc.postValue(it)
                hideLoading()
            }
        } catch (e: Exception) {
            errorHandler(Exception("Tmc code is not presented"))
            hideLoading()
        }
    }
}