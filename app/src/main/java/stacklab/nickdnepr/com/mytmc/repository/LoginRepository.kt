package stacklab.nickdnepr.com.mytmc.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class LoginRepository {

    fun tryAuth(login: String, password: String, onComplete: (Boolean) -> Unit) {
        CoroutineScope(Dispatchers.Default).launch {
            delay(500)
            val result = login != "invalid"

            onComplete(result)
        }
    }

}