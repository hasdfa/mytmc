package stacklab.nickdnepr.com.mytmc.utils

import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.springframework.web.client.RestTemplate

object Dispatcher {

    private const val protocol = "http"
    private const val domain = "31.131.27.228"
    private const val port = "80"
    private const val basePath = "/rest/Mansions"

    private const val baseUrl = "$protocol://$domain:$port$basePath"

    fun<T> runAsync(path: String,
                            onComplete: (T) -> Unit,
                            onError: (Exception) -> Unit,
                            block: Runnable.() -> Pair<T?, Exception?>
    ) {
        runAsync(path) {
            val (result, err) = block()
            Log.d("app", result?.toString() ?: "<null>")
            Log.d("app", err?.message ?: "<null>")

            runMain {
                if (result != null)
                    onComplete(result)
                else
                    onError(err ?: NullPointerException())
            }
        }
    }

    fun runAsync(path: String,
                        onComplete: () -> Unit,
                        onError: (Exception) -> Unit,
                        block: Runnable.() -> Pair<Any?, Exception?>
    ) {
        runAsync(path) {
            val (result, err) = block()
            Log.d("app", result?.toString() ?: "<null>")
            Log.d("app", err?.message ?: "<null>")

            runMain {
                if (err == null)
                    onComplete()
                else
                    onError(err)
            }
        }
    }

    private fun runMain(block: () -> Unit): Job {
        return CoroutineScope(Dispatchers.Main).launch {
            block()
        }
    }

    private fun runAsync(path: String, block: Runnable.() -> Unit): Job {
        return CoroutineScope(Dispatchers.Default).launch {
            block(
                Runnable(
                    url = baseUrl + path,
                    restTemplate = RestTemplate()
                )
            )
        }
    }
}