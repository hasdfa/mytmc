package stacklab.nickdnepr.com.mytmc.models

import java.io.Serializable

data class Tmc(
    var id: Long = 0,
    var location_id: Long = 0,
    var description: String? = null,
    var items: String,
    var code: AbstractCode,
    var article: String?,
    var comments: String?,
    var supplier: String?
) : Serializable
