package stacklab.nickdnepr.com.mytmc.repository

import com.google.gson.reflect.TypeToken
import org.springframework.util.FileCopyUtils
import stacklab.nickdnepr.com.mytmc.models.AbstractCode
import stacklab.nickdnepr.com.mytmc.models.PhotoModel
import stacklab.nickdnepr.com.mytmc.models.Tmc
import stacklab.nickdnepr.com.mytmc.utils.Dispatcher
import stacklab.nickdnepr.com.mytmc.utils.JObjectMap
import java.io.File
import java.lang.reflect.Type

class TmcRepository {

    private val listType: Type = object : TypeToken<List<Tmc>>() {}.type
    private val tmcType: Type = object : TypeToken<Tmc>() {}.type
    private val codeType: Type = object : TypeToken<AbstractCode>() {}.type
    private val imagesType: Type = object : TypeToken<List<String>>() {}.type

    var handleError: (Exception) -> Unit = {
        it.printStackTrace()
    }

    fun getTmcList(onComplete: (List<Tmc>) -> Unit) {
        Dispatcher.runAsync("/getTmcList", onComplete, handleError) {
            get(listType)
        }
    }

    fun getTmcByCode(code: String, onComplete: (Tmc) -> Unit) {
        Dispatcher.runAsync("/getTmc/$code", onComplete, handleError) {
            get(tmcType)
        }
    }

    fun attachCode(tmc: Tmc, onComplete: (Tmc) -> Unit) {
        Dispatcher.runAsync("/attachCode", onComplete, handleError) {
            post(tmc, tmcType)
        }
    }

    fun getCode(code: String, onComplete: (AbstractCode) -> Unit) {
        Dispatcher.runAsync("/getCode/$code", onComplete, handleError) {
            get(codeType)
        }
    }

    fun addPhoto(tmcId: Long, photo: File, onComplete: () -> Unit) {
        Dispatcher.runAsync("/addPhoto", onComplete, handleError) {
            val arr = FileCopyUtils.copyToByteArray(photo)
            val model = PhotoModel(tmcId, photo.name, arr)

            return@runAsync post(model)
        }
    }

    fun loadImages(tmcId: Long, onComplete: (List<String>) -> Unit) {
        Dispatcher.runAsync("/getTmcPhotoList/$tmcId", onComplete, handleError) {
            get(imagesType)
        }
    }

}