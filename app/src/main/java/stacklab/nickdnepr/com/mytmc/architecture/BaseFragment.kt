package stacklab.nickdnepr.com.mytmc.architecture

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.Fragment

abstract class BaseFragment(private val layoutResourceId: Int) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(layoutResourceId, container, false)!!

    fun showToast(text: String?){
        (activity as BaseActivity).showToast(text)
    }

    fun showToast(text: String, duration: Int){
        (activity as BaseActivity).showToast(text, duration)
    }

    fun showAlert(title: String?, message: String? = null) {
        val alertBuilder = AlertDialog.Builder(ContextThemeWrapper(context, stacklab.nickdnepr.com.mytmc.R.style.AlertDialogCustom))
        alertBuilder.setTitle(title)
        alertBuilder.setMessage(message)

        val alert = alertBuilder.create()
        alert.setButton(AlertDialog.BUTTON_POSITIVE, "ОК") { _, _ ->
            alert.cancel()
        }
        alert.show()
    }
}