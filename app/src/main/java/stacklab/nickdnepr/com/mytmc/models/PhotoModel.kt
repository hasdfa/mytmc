package stacklab.nickdnepr.com.mytmc.models

import android.util.Base64
import stacklab.nickdnepr.com.mytmc.utils.JsonConvertible
import java.util.*

data class PhotoModel(
    val tmc_id: Long,
    val filename: String,
    val file: ByteArray
): JsonConvertible {

    override fun toJson(): String {
        return """{"tmc_id":%d,"filename":"%s","file":%s}"""
            .format(tmc_id, filename, Arrays.toString(file))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other)
            return true
        if (javaClass != other?.javaClass)
            return false

        if (other !is PhotoModel)
            return false

        if (tmc_id != other.tmc_id)
            return false
        if (filename != other.filename)
            return false

        return true
    }

    override fun hashCode(): Int {
        var result = tmc_id.hashCode()
        result = 31 * result + filename.hashCode()
        return result
    }
}