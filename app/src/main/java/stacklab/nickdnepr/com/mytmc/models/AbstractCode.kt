package stacklab.nickdnepr.com.mytmc.models

import java.io.Serializable
import java.util.*

data class AbstractCode(
    val id: Long,
    val date: Date = Date(),
    val login: String = "",
    val textCode: String
): Serializable