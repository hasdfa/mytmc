package stacklab.nickdnepr.com.mytmc.architecture

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    val progressEvent = MutableLiveData<Boolean>()

    init {
        progressEvent.postValue(false)
    }

    protected fun showLoading() {
        progressEvent.postValue(true)
    }

    protected fun hideLoading() {
        progressEvent.postValue(false)
    }
}