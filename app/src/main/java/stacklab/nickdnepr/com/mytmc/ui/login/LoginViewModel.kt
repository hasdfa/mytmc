package stacklab.nickdnepr.com.mytmc.ui.login

import androidx.lifecycle.MutableLiveData
import stacklab.nickdnepr.com.mytmc.architecture.BaseViewModel
import stacklab.nickdnepr.com.mytmc.repository.LoginRepository

class LoginViewModel(private val repository: LoginRepository) : BaseViewModel(){

    val loginSuccess = MutableLiveData<Boolean>()

    init {
//        loginSuccess.postValue(false)
    }

    fun login(login:String, password:String){
        showLoading()
        repository.tryAuth(login, password){
            loginSuccess.postValue(it)
            hideLoading()
        }
    }

}