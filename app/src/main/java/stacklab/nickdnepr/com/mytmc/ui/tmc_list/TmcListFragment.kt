package stacklab.nickdnepr.com.mytmc.ui.tmc_list

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_tmc_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import stacklab.nickdnepr.com.mytmc.R
import stacklab.nickdnepr.com.mytmc.architecture.BaseMVVMFragment
import stacklab.nickdnepr.com.mytmc.models.Tmc
import stacklab.nickdnepr.com.mytmc.ui.details.TmcDetailsFragment
import stacklab.nickdnepr.com.mytmc.ui.main.MainActivity
import stacklab.nickdnepr.com.mytmc.ui.scanner.ScannerFragment
import java.lang.Exception

class TmcListFragment : BaseMVVMFragment(R.layout.fragment_tmc_list) {

    private val viewModel: TmcListViewModel by viewModel()
    private lateinit var adapter: TmcListAdapter
    private val list = ArrayList<Tmc>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setErrorHandler(handleError)

        adapter = TmcListAdapter(activity!!.applicationContext, list) {
            openDetails(it)
        }
        recycler.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        viewModel.getList()
        recycler.adapter = adapter
        refresh.setOnRefreshListener {
            viewModel.getList()
        }
        recognize_tmc_button.setOnClickListener {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), 2)
        }
    }

    private var handleError: (Exception) -> Unit = {
        showAlert("Network error", it.message)
    }

    override fun onBindLiveData() {
        super.onBindLiveData()

        observe(viewModel.tmcList) {
            list.clear()
            list.addAll(it)
            adapter.notifyDataSetChanged()
            refresh.isRefreshing = false
            Log.i("TmcList", list.toString())
        }
        observe(viewModel.processedCode) { code ->
            Log.i("Barcode", "processed $code")
            viewModel.getTmcByCode(code)
        }
        observe(viewModel.receivedTmc) {
            Log.i("Tmc", it.toString())
            if (it.id != 0L) {
                openDetails(it)
            } else {
                showToast("Tmc with that code not found", Toast.LENGTH_LONG)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var okToStart = true
        grantResults.forEach {
            if (it == PackageManager.PERMISSION_DENIED){
                okToStart=false
            }
            if (okToStart){
                (activity as MainActivity).changeFragment(ScannerFragment(), true)
            } else {
                showAlert("Permission denied")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.subscribe()
    }

    override fun onPause() {
        super.onPause()
        refresh.isRefreshing = false
    }

    fun openDetails(tmc: Tmc) {
        viewModel.unsubscribe()
        val fragment = TmcDetailsFragment()
        val arguments = Bundle()
        arguments.putSerializable("Tmc", tmc)
        fragment.arguments = arguments
        (activity as MainActivity).changeFragment(fragment, true)
    }
}