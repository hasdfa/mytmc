package stacklab.nickdnepr.com.mytmc.architecture

import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

abstract class BaseMVVMFragment(layoutResourceId: Int):BaseFragment(layoutResourceId){

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        onBindLiveData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onBindLiveData()
    }

    protected open fun onBindLiveData(){

    }

    fun<T, LD: LiveData<T>> observe(liveData:LD, onChanged:(T)->Unit){
        liveData.observe(this, Observer { value->
            value?.let(onChanged)
        })
    }
}