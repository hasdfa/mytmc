package stacklab.nickdnepr.com.mytmc.architecture

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper

abstract class BaseActivity(@LayoutRes private val layoutResourceId: Int) : AppCompatActivity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResourceId)
    }

    fun showToast(text: String?) {
        showToast(text, Toast.LENGTH_SHORT)
    }

    fun showToast(text: String?, duration:Int) {
        Toast.makeText(this, text, duration).show()
    }

    fun showAlert(title: String?, message: String?) {
        val alertBuilder = AlertDialog.Builder(ContextThemeWrapper(this, stacklab.nickdnepr.com.mytmc.R.style.AlertDialogCustom))
        alertBuilder.setTitle(title)
        alertBuilder.setMessage(message)

        val alert = alertBuilder.create()
        alert.setButton(AlertDialog.BUTTON_POSITIVE, "ОК") { _, _ ->
            alert.cancel()
        }
        alert.show()
    }
}