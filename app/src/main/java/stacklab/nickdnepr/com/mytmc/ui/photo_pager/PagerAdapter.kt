package stacklab.nickdnepr.com.mytmc.ui.photo_pager

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class PagerAdapter(val images:List<String>,fragmentManager: FragmentManager) :FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        val arguments = Bundle()
        arguments.putString("link", images[position])
        val fragment = PageFragment()
        fragment.arguments=arguments
        return fragment
    }

    override fun getCount() = images.size
}