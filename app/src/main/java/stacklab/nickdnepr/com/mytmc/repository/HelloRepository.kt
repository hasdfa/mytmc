package stacklab.nickdnepr.com.mytmc.repository

class HelloRepository {

    fun sayHello() = "Hello ${System.currentTimeMillis()}"
}