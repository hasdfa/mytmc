package stacklab.nickdnepr.com.mytmc.ui.details

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.core.content.FileProvider
import com.bumptech.glide.*
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.fragment_tmc_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import stacklab.nickdnepr.com.mytmc.R
import stacklab.nickdnepr.com.mytmc.architecture.BaseMVVMFragment
import stacklab.nickdnepr.com.mytmc.models.Tmc
import stacklab.nickdnepr.com.mytmc.ui.main.MainActivity
import stacklab.nickdnepr.com.mytmc.ui.photo_pager.PhotoPagerFragment
import stacklab.nickdnepr.com.mytmc.ui.scanner.ScannerFragment
import java.io.File
import java.lang.Exception
import java.util.*

class TmcDetailsFragment : BaseMVVMFragment(R.layout.fragment_tmc_details) {

    private val viewModel: TmcDetailsViewModel by viewModel()
    private lateinit var tmc: Tmc

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.errorHandler = handleError
        tmc = arguments?.getSerializable("Tmc") as Tmc
    }

    private var handleError: (Exception) -> Unit = {
        showAlert("Network error", it.message)
        refresh.isRefreshing = false
    }

    override fun onResume() {
        super.onResume()
        requestPreview()
    }

    override fun onBindLiveData() {
        super.onBindLiveData()
        observe(viewModel.codeProcessed) {
            viewModel.getCode(it)
        }
        observe(viewModel.codeReceived) {
            if (it.id != 0L) {
                tmc.code = it
                viewModel.attachCode(tmc)
            } else {
                showToast("Barcode not found")
            }
        }
        observe(viewModel.tmcUpdated) {
            showData()
        }
        observe(viewModel.progressEvent) {
            progress.visibility = if (it) View.VISIBLE else View.INVISIBLE
        }
        observe(viewModel.imagesLoaded) {
            val arguments = Bundle()
            arguments.putStringArrayList("links", it as ArrayList<String>?)
            val fragment = PhotoPagerFragment()
            fragment.arguments = arguments
            (activity as MainActivity).changeFragment(fragment, true)
        }
        observe(viewModel.previewLoaded) {
            Glide.with(this)
                .load("http://31.131.27.228/rest/Mansions/getTmcPhoto/$it")
                //.diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(object :RequestListener<Drawable>{
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        showToast("Could not load preview")
                        image.setBackgroundResource(R.drawable.camera)
                        img_progress.visibility=View.GONE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        img_progress.visibility=View.GONE
                        return false
                    }

                })
                .into(image)
        }
        observe(viewModel.photoAdded) {
            //            viewModel.loadImages(tmc.id)
        }
        observe(viewModel.refreshedTmc){
            tmc = it
            requestPreview()
            showData()
            refresh.isRefreshing=false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showData()

        attach_code_btn.setOnClickListener {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_SCANNER)

        }
        add_photo_btn.setOnClickListener {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_PHOTO
            )
        }
        image.setOnClickListener {
            viewModel.loadImages(tmc.id)
        }
        refresh.setOnRefreshListener {
            viewModel.refreshTmc(tmc)
        }
    }

    private fun showData() {
        if (tmc.code?.id != 0L) {
            attach_code_btn.visibility = View.GONE
        }

        tmc_title.text = tmc.items
        description_body.text = tmc.description ?: "No description available"
        article_body.text = tmc.article ?: "No article available"
        comments_body.text = tmc.comments ?: "No comments available"
        supplier_body.text = tmc.supplier ?: "No supplier available"

        code.text = if (tmc.code?.id == 0L) "Not assigned" else tmc.code?.textCode

    }

    @SuppressLint("SdCardPath")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CODE_IMAGE && resultCode == RESULT_OK) {
            val file = File("${Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)}/picFolder/img.jpg")
            val uri = Uri.fromFile(file)
//            showToast(file.exists().toString())
            viewModel.addPhoto(tmc.id, file)
        }
    }

    private fun makePhoto() {
        val photo = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val dir = "${Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)}/picFolder/"
        val newdir = File(dir)
        newdir.mkdirs()
        val file = "${dir}img.jpg"
        val newfile = File(file)
        newfile.createNewFile()
//        showToast(newfile.absolutePath)
        Log.i("TempFile", newfile.absolutePath)
        val uri = FileProvider.getUriForFile(activity!!.applicationContext, ".provider", newfile)

        photo.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri)

        startActivityForResult(photo, CODE_IMAGE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var okToStart = true
        grantResults.forEach {
            if (it == PackageManager.PERMISSION_DENIED) {
                okToStart = false
            }
        }
        if (okToStart) {
            if (requestCode == REQUEST_PHOTO) {
                makePhoto()
            } else if (requestCode == REQUEST_SCANNER) {
                (activity as MainActivity).changeFragment(ScannerFragment(), true)
            }
        } else {
            showToast("Permission Denied")
        }
    }

    private fun requestPreview(){
        img_progress.visibility=View.VISIBLE
        viewModel.loadPreview(tmc.id)
    }

    companion object {
        private const val CODE_IMAGE = 5
        private const val REQUEST_SCANNER = 0
        private const val REQUEST_PHOTO = 1
    }
}