package stacklab.nickdnepr.com.mytmc.ui.photo_pager

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.fragment_page.*
import stacklab.nickdnepr.com.mytmc.R
import stacklab.nickdnepr.com.mytmc.architecture.BaseMVVMFragment

class PageFragment:BaseMVVMFragment(R.layout.fragment_page){

    override fun onResume() {
        super.onResume()
        progress.visibility=View.VISIBLE
        val url = "http://31.131.27.228/rest/Mansions/getTmcPhoto/${arguments?.getString("link")!!}"
        val circularProgressDrawable = CircularProgressDrawable(activity?.applicationContext!!)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        Glide.with(this).load(url).listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                showToast("Error while loading image")
                return false
            }
            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                progress.visibility=View.GONE
                return false
            }
        }).into(image)
    }
}