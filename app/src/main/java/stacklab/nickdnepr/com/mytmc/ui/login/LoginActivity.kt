package stacklab.nickdnepr.com.mytmc.ui.login

import android.content.Intent
import android.opengl.Visibility
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import stacklab.nickdnepr.com.mytmc.R
import stacklab.nickdnepr.com.mytmc.architecture.BaseMVVMActivity
import stacklab.nickdnepr.com.mytmc.ui.main.MainActivity

class LoginActivity : BaseMVVMActivity(R.layout.activity_login) {

    val viewModel: LoginViewModel by viewModel()

    override fun onBindLiveData() {
        observe(viewModel.progressEvent) {
            progress.visibility = if (it) View.VISIBLE else View.INVISIBLE
        }
        observe(viewModel.loginSuccess) {
            if (it) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            } else {
                // TODO fix toast showing bug
                showAlert("Invalid credentials", "Invalid credentials, try again")
                password.text?.clear()
            }
        }

        login_button.setOnClickListener {
            viewModel.login(login.text.toString(), password.text.toString())
        }
    }
}